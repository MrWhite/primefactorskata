class primeFactor(){
    fun calculate(i: Int): IntArray {

        val primeArray:ArrayList<IntArray> =arrayListOf(intArrayOf(1),intArrayOf(2),intArrayOf(3),intArrayOf(2,2),intArrayOf(5),intArrayOf(2,3))
        val list= arrayListOf<Int>()
        var ongoingNumber= i


        while(ongoingNumber!=1){
            val actualValue=findFirstPrimeDivisor(ongoingNumber)
            ongoingNumber= ongoingNumber/actualValue
            list += listOf(actualValue)

        }

        return list.toIntArray()

    }


}
fun findFirstPrimeDivisor(number:Int) :Int{
    for(pointer in 2..number){
        if(isPrimeNumber(pointer)){
            if(number%pointer==0){
                return pointer

            }

        }

    }
    return 0
}


fun isPrimeNumber(number: Int): Boolean {

    for (i in 2..number / 2) {
        if (number % i == 0) {
            return false
        }
    }
    return true
}


fun main(args: Array<String>) {
    println("Hello World!")
}