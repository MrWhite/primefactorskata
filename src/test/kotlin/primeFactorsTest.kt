import org.junit.jupiter.api.Test

class primeFactorsTest {

    @Test
    fun primeFactorof2returns2() {

        val primeFactors = primeFactor()
        assert(primeFactors.calculate(2).contentEquals(intArrayOf(2)))
    }

    @Test
    fun primeFactorof3returns3() {

        val primeFactors = primeFactor()
        assert(primeFactors.calculate(3).contentEquals(intArrayOf(3)))
    }

    @Test
    fun primeFactorof4returns22() {

        val primeFactors = primeFactor()
        assert(primeFactors.calculate(4).contentEquals(intArrayOf(2,2)))
    }

    @Test
    fun primeFactorof6returns23() {

        val primeFactors = primeFactor()
        assert(primeFactors.calculate(6).contentEquals(intArrayOf(2,3)))
    }

    @Test
    fun primeFactorof9returns33() {

        val primeFactors = primeFactor()
        assert(primeFactors.calculate(9).contentEquals(intArrayOf(3,3)))
    }

    @Test
    fun primeFactorof12returns223() {

        val primeFactors = primeFactor()
        assert(primeFactors.calculate(12).contentEquals(intArrayOf(2,2,3)))
    }

    @Test
    fun primeFactorof15returns35() {

        val primeFactors = primeFactor()
        assert(primeFactors.calculate(15).contentEquals(intArrayOf(3,5)))
    }
}